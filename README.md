# README #

Copy header file into project directory and insert:

*#include "SBUS.h"*

in your code.


**Warning:**

Dont use the hardware Serial port from Arduino if you include this library!


**Example:**


```
#!c++

#include "SBUS.h"

SBUS sbus;

void setup()
{
  sbus.Start();
  sbus.SetChannel(0, 768); //Set Channel 0 to 768 (from 1023)
  sbus.SetChannel(1, 768);
  sbus.SetChannel(2, 768);
  sbus.SetChannel(3, 256);
}

void loop()
{
  //Change Channel values
  sbus.SetChannel(2, 256);
  //Build data to write
  sbus.BuildData();
  //Write data
  sbus.WriteData();
}
```